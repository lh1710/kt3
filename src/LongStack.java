import java.util.ArrayList;
import java.util.*;


public class LongStack {

    private LinkedList<Long> stringList = new LinkedList<>();


   public static void main (String[] argum) {
      // TODO!!! Your tests here!
   }

   LongStack() {

   }

   @Override
   public Object clone() throws CloneNotSupportedException {
       LongStack x = new LongStack();
       x.stringList = new LinkedList<>(stringList);
      return x;
   }

   public boolean stEmpty() {
      return stringList.isEmpty();
   }

   public void push (long a) {
     stringList.add(a);
   }

   public long pop()  {
       if (stringList.size() == 0) {
           try {
               throw new Exception("stringList is empty!");
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
      Long x = stringList.getLast();
      stringList.remove(stringList.size()-1);
      return x;
   }

   public void op (String s) {

       if (!s.equals("+") && !s.equals("-") && !s.equals("/") && !s.equals("*")) {
           throw new RuntimeException("Cannot perform operation with " + s);
       }
       ArrayList<String> list = new ArrayList<>();
       for (Long element : stringList) {
           list.add(element.toString());
       }
       list.add(s);
       if (list.size() < 2) {
           try {
               throw new Exception("stringList is too short!");
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
       stringList.clear();
       stringList.add(listToBeAdded(list));
   }

   public long tos() {
       if (stringList.size() == 0) {
           try {
               throw new Exception("stringList is empty!");
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
      return stringList.getLast();
   }

   @Override
   public boolean equals (Object o) {
       if (o == this) {
           return true;
       }

       if (!(o instanceof LongStack)) {
           return false;
       }

       LongStack c = (LongStack) o;

       if (stringList.size() != c.stringList.size()) {
           return false;
       }

       for (int i = 0; i < stringList.size(); i++) {
           if (!stringList.get(i).equals(c.stringList.get(i))) {
               return false;
           }
       }
       return true;
   }

   @Override
   public String toString() {
       StringBuilder concatenatedString = new StringBuilder();
       String delimiter = " ";
       for (long word : stringList) {
           concatenatedString.append(concatenatedString.toString().equals("") ? word : delimiter + word);
       }
       return concatenatedString.toString();
   }

   public static long interpret(String pol) {
       if (pol.equals("")) {
           throw new RuntimeException("Empty expression");
       }
       ArrayList<String> list2 = new ArrayList<>();
       List<String> list = new ArrayList<>(Arrays.asList(pol.split(" ")));
       list.removeAll(Arrays.asList("", "\t", "\n"));

       for (String value : list) {
           if (isNumeric(value.trim()) || value.equals("+") || value.equals("-")
                   || value.equals("/") || value.equals("*") || value.equals("SWAP") || value.equals("ROT")) {
               list2.add(value.trim());
           } else {
               throw new RuntimeException("Illegal sign " + value + "in string " + pol);
           }
       }

       int xv = countSigns(list2).size();
       int y = countNumbers(list2).size();

       if (countSigns(list2).size() >= countNumbers(list2).size()) {
           String x = countSigns(list2).get(countSigns(list2).size() - 1);
           throw new RuntimeException("Cannot perform " + x + " in expression " + pol);
       }
       if (countNumbers(list2).size() - countSigns(list2).size() > 1) {
           throw new RuntimeException("Too many numbers in expression " + pol);
       }

       LongStack stack = new LongStack();
       for(String ch : list2){
           switch (ch) {
               case "+": {
                   Long num1 = stack.pop();
                   Long num2 = stack.pop();
                   stack.push(num1 + num2);
                   break;
               }
               case "-": {
                   Long num1 = stack.pop();
                   Long num2 = stack.pop();
                   stack.push(num2 - num1);
                   break;
               }
               case "*": {
                   Long num1 = stack.pop();
                   Long num2 = stack.pop();
                   stack.push(num1 * num2);
                   break;
               }
               case "/": {
                   Long num1 = stack.pop();
                   Long num2 = stack.pop();
                   stack.push(num2 / num1);
                   break;
               }
               case "ROT": {
                   stack.rot();
                   break;
               }
               case "SWAP": {
                   stack.swap();
                   break;
               }
               default:
                   stack.push(Long.parseLong(ch));
                   break;
           }
       }
       return stack.pop();

   }

   public void rot() {
       if (stringList.size() < 3) {
           throw new RuntimeException("Not enough elements in list");
       }
       long firstElement = pop();
       long secondElement = pop();
       long thirdElement = pop();

       push(secondElement);
       push(firstElement);
       push(thirdElement);

   }

    public void swap() {
        if (stringList.size() < 2) {
            throw new RuntimeException("Not enough elements in list");
        }
        long firstElement = pop();
        long secondElement = pop();
        push(firstElement);
        push(secondElement);
    }



    public static Long listToBeAdded(ArrayList<String> list) { // https://www.thecodingshala.com/2019/10/how-to-evaluate-reverse-polish-notation.html
       Stack<Long> st = new Stack<>();
       for(String ch : list){
           switch (ch) {
               case "+": {
                   Long num1 = st.pop();
                   Long num2 = st.pop();
                   st.push(num1 + num2);
                   break;
               }
               case "-": {
                   Long num1 = st.pop();
                   Long num2 = st.pop();
                   st.push(num2 - num1);
                   break;
               }
               case "*": {
                   Long num1 = st.pop();
                   Long num2 = st.pop();
                   st.push(num1 * num2);
                   break;
               }
               case "/": {
                   Long num1 = st.pop();
                   Long num2 = st.pop();
                   st.push(num2 / num1);
                   break;
               }
               case "rot": {

               }
               default:
                   st.push(Long.parseLong(ch));
                   break;
           }
       }
       return st.pop();
   }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            long d = Long.parseLong(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static ArrayList<String> countNumbers(ArrayList<String> list2) {
        ArrayList<String> numbers = new ArrayList<>();

        for (String s : list2) {
            if (isNumeric(s)) {
                numbers.add(s);
            }
        }
        return numbers;
    }

    public static ArrayList<String> countSigns(ArrayList<String> list2) {
        ArrayList<String> signs = new ArrayList<>();

        for (String s : list2) {
            if (!isNumeric(s) && !s.equals("SWAP") && !s.equals("ROT")) {
                signs.add(s);
            }
        }
        return signs;
    }

}

